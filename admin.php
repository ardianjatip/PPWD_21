<?php
	function connectDB() {
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "tugas_akhir";
		
		// Create connection
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		
		// Check connection
		if (!$conn) {
			die("Connection failed: " + mysqli_connect_error());
		}
		return $conn;
	}
	
	function insertBook() {
		$conn = connectDB();
		
		$img_path = $_POST['img_path'];
		$title = $_POST['title'];
		$author = $_POST['author'];
		$publisher = $_POST['publisher'];
		$description = $_POST['description'];
		$quantity = $_POST['quantity'];

		$checkTitle = "SELECT * FROm book WHERE title = $title";
		$result = mysql_query($checkTitle);

		if ($result && mysql_num_rows($result) > 0) {
			$sql = "INSERT into book (img_path, title, author, publisher, description, quantity) values('$img_path','$title','$author','$publisher','$description','$quantity')";
    		echo "test";
		} else {
			
    		$sql = "UPDATE book SET quantity = quantity+$quantity WHERE title='$title'";
			echo "test2";
		}
		
		if($result = mysqli_query($conn, $sql)) {
			echo "New record created successfully <br/>";
			header("Location: admin.php");
			} else {
			die("Error: $sql");
		}
		mysqli_close($conn);
	}
	
	
	function selectAllFromTable($table) {
		$conn = connectDB();
		
		$sql = "SELECT book_id, img_path, title, publisher, author, description, quantity FROM $table";
		
		if(!$result = mysqli_query($conn, $sql)) {
			die("Error: $sql");
		}
		mysqli_close($conn);
		return $result;
	}
	
	function detailBook($book){
		$conn = connectDB();
		$book_id = $book;
		
		header("Location: detail.php");
		
		mysqli_close($conn);
	}
	
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		if($_POST['command'] === 'insert') {
			insertBook();
		} else if($_POST['command'] === 'delete') {
			deleteBook($_POST['book_id']);
		}else if($_POST['command'] === 'detail') {
			session_start();
			$_SESSION['book_id'] = $_POST['book_id'];
			detailBook($_POST['book_id']);
		}
	}
	
	
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bookoo</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="src/css/style.css">
	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
		  	<div class="container-fluid">
		  		<div class="navbar-header">
		      		<a class="navbar-brand" href="#">Bookoo</a>
		    	</div>
		    	<ul class="nav navbar-nav navbar-right">
		    		<li>
		    			<a href="#">
						Welcome,
						<?php
							session_start();
							if (!isset($_SESSION["username"])){
								header("Location: index.php");
							}
							echo $_SESSION["username"];
						?>
						!
						</a>
					</li>
		      		<li>
		      			<a href="logout.php">
		      				<span class="glyphicon glyphicon-log-out"></span> Log Out
		      			</a>
		      		</li>
		    	</ul>
		  	</div>
		</nav>
		<div class="container" id="div2">
			<h2 class="text-center">Bookoo</h2>
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#insertModal">+ Add New Book
			</button>
			<h4>Book List</h4>
			<div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="insertModalLabel">Add Book</h4>
						</div>
						<div class="modal-body">
							<form action="admin.php" method="post">
								<div class="form-group">
									<label for="title">Title</label>
									<input type="text" class="form-control" id="insert-title" name="title" placeholder="Title" required>
								</div>
								<div class="form-group">
									<label for="image_path">Image Path</label>
									<input type="text" class="form-control" id="insert-image_path" name="image_path" placeholder="Image path">
								</div>
								<div class="form-group">
									<label for="author">Author</label>
									<input type="text" class="form-control" id="insert-author" name="author" placeholder="Author" required>
								</div>
								<div class="form-group">
									<label for="publisher">Publisher</label>
									<input type="text" class="form-control" id="insert-publisher" name="publisher" placeholder="Publisher" required>
								</div>
								<div class="form-group">
									<label for="description">Description</label>
									<input type="text" class="form-control" id="insert-description" name="description" placeholder="Description" required>
								</div>
								<div class="form-group">
									<label for="description">Quantity</label>
									<input type="number" class="form-control" id="insert-quantity" name="quantity" placeholder="Quantity" min="1" required>
								</div>
								<input type="hidden" id="insert-command" name="command" value="insert">
								<button type="submit" class="btn btn-primary">Submit</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="table-responsive">
				<table class='table table-hover '>
					<thead > <tr> <th>ID</th> <th>Image</th> <th>Title</th> <th>Publisher</th> <th>Author</th> <th>Description</th> <th>Quantity</th> </tr> </thead>
					<tbody>
						<?php
							$books = selectAllFromTable("book");
							while ($row = mysqli_fetch_row($books)) {
								echo "<tr>";
								echo
								'<td class="table-bordered"><p>'.$row[0].'</p></td>
								<td class="table-bordered">
									<form action="user.php" method="post">
										<input type="hidden" id="detail-command" name="command" value="detail">
										<input type="hidden" id="detail-command" name="book_id" value="'.$row[0].'">
										<button type="submit">
										<img src="'.$row[1].'" alt="Image not available" style="width:100px">
										</button>
									</form>
								</td>
								<td class="table-bordered"><p>'.$row[2].'</p></td>
								<td class="table-bordered"><p>'.$row[3].'</p></td>
								<td class="table-bordered"><p>'.$row[4].'</p></td>
								<td class="table-bordered" style="text-align: justify;"><p>'.$row[5].'</p></td>
								<td class="table-bordered"><p>'.$row[6].'</p></td>';

								echo "</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>							
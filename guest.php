<?php
	session_start();
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "tugas_akhir";
	$isAdmin = false;
	$isUser = false;

	// Create connection
	$conn = mysqli_connect($servername, $username, $password, $dbname);

	// Check connection
	if (!$conn) {
		die("Connection failed: " + mysqli_connect_error());
	}

	if($_SERVER["REQUEST_METHOD"] === "POST") {
		$username_input = $_POST["username"];
		$password_input = $_POST["password"];

		$sql_login = "SELECT * FROM user";
		$result = mysqli_query($conn, $sql_login);

		if(mysqli_num_rows($result) > 0) {
			while ($row = mysqli_fetch_assoc($result)) {
				if($username_input === $row["username"] && $password_input === $row["password"]) {
					if($row["role"] === "admin") {
						$_SESSION["username"] = $row["username"];
						$isAdmin = true;
						break;
					}
					else if($row["role"] === "user") {
						$_SESSION["username"] = $row["username"];
						$isUser = true;
						break;	
					}
				}
			}

			if($isAdmin) {
				header("Location: admin.php");
			} else if($isUser) {
				header("Location: user.php");
			} else {
				echo  "<script type='text/javascript'>alert('Login Gagal');</script>";
			}
		}	
	}

	function connectDB() {
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "tugas_akhir";
		
		// Create connection
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		
		// Check connection
		if (!$conn) {
			die("Connection failed: " + mysqli_connect_error());
		}
		return $conn;
	}

	function selectAllFromTable($table) {
		$conn = connectDB();
		
		$sql = "SELECT book_id, img_path, title, publisher, author, description, quantity FROM $table";
		
		if(!$result = mysqli_query($conn, $sql)) {
			die("Error: $sql");
		}
		mysqli_close($conn);
		return $result;
	}
	
			
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bookoo</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="src/css/style.css">
	</head>
	<body>
		<nav class="navbar navbar-fixed-top navbar-default">
		  	<div class="container-fluid">
		  		<div class="navbar-header">
		      		<a class="navbar-brand" href="#">Bookoo</a>
		    	</div>
		    	<ul class="nav navbar-nav navbar-right">
		      		<li><button id="login-btn" type="button" class="btn" data-toggle="modal" data-target="#loginModal">Login</button></li>
		    	</ul>
		  	</div>
		</nav>
		<div class="modal fade" id="loginModal" tabindex="0" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="insertModalLabel">Welcome! Please Login</h4>
					</div>
					<div class="modal-body">
						<form action="guest.php" method="post">
							<div class="form-group">
								<label for="username">Username</label>
								<input type="text" class="form-control" id="insert-username" name="username" placeholder="Username">
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" class="form-control" id="insert-password" name="password" placeholder="Password">
							</div>
							<input type="hidden" id="insert-command" name="command" value="insert">
							<button type="submit" class="btn btn-info">Login</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="container" id="div2">
			<h2 class="text-center">Bookoo</h2>
			
			<h4>Book List</h4>
			
			<div class="table-responsive">
				<table class='table table-hover '>
					<thead > <tr> <th>No.</th> <th>Image</th> <th>Title</th> <th>Publisher</th> <th>Author</th> <th>Description</th> <th>Quantity</th> </tr> </thead>
					<tbody>
						<?php
							$books = selectAllFromTable("book");
							while ($row = mysqli_fetch_row($books)) {
								echo "<tr>";
								echo
								'<td class="table-bordered"><p>'.$row[0].'</p></td>
								<td class="table-bordered"><img src="'.$row[1].'" alt="Image not available" style="width:100px"></td>
								<td class="table-bordered"><p>'.$row[2].'</p></td>
								<td class="table-bordered"><p>'.$row[3].'</p></td>
								<td class="table-bordered"><p>'.$row[4].'</p></td>
								<td class="table-bordered" style="text-align: justify;"><p>'.$row[5].'</p></td>
								<td class="table-bordered"><p>'.$row[6].'</p></td>';
								echo "</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>							
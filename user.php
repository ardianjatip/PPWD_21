<?php
	function connectDB() {
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "tugas_akhir";
		
		// Create connection
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		
		// Check connection
		if (!$conn) {
			die("Connection failed: " + mysqli_connect_error());
		}
		return $conn;
	}
	
	function updateMyBook($book_id){
		$conn = connectDB();

		$split = explode("\n", $book_id);
		$user_id = $split[1];
		$book_loan = $split[0];
		
		$sql = "INSERT INTO loan (book_id, user_id) VALUES ('$book_loan', '$user_id')";
		
		if($result = mysqli_query($conn, $sql)) {
			echo "New record created successfully <br/>";
			header("Location: user.php");
			} else {
			die("Error: $sql");
		}
		mysqli_close($conn);
	}

	function rentBook($book_id, $quantity) {
		$conn = connectDB();
		
		$split = explode("\n", $book_id);
		$user_id = $split[1];
		$book_loan = $split[0];

		$sql = "UPDATE book SET quantity=$quantity WHERE book_id=$book_loan";
		
		if($result = mysqli_query($conn, $sql)) {
			echo "New record created successfully <br/>";
			header("Location: user.php");
			} else {
			die("Error: $sql");
		}
		mysqli_close($conn);
	}
	
	function selectAllFromTable($table) {
		$conn = connectDB();
		
		$sql = "SELECT book_id, img_path, title, publisher, author, description, quantity FROM $table";
		
		if(!$result = mysqli_query($conn, $sql)) {
			die("Error: $sql");
		}
		mysqli_close($conn);
		return $result;
	}

	function checkBook($book_id){
		$conn = connectDB();
		$userid = $_SESSION["user_id"];
		$sql = "SELECT book.book_id, book.img_path, book.title, book.publisher, book.author, book.description, book.quantity FROM `loan` INNER JOIN `book` on loan.book_id=book.book_id WHERE loan.user_id = $userid AND loan.book_id = $book_id";
		$result = mysqli_query($conn, $sql);

		if(mysqli_num_rows($result) > 0) {
			return true;
		} else {
			return false;
		}	
	}

	function selectReview($book_id){
		$conn = connectDB();
		
		$sql = "SELECT review.review_id, user.username, review.date, review.content FROM `review` INNER JOIN `book` on review.book_id = book.book_id INNER JOIN `user` on review.user_id = user.user_id WHERE review.book_id = '$book_id'";
		
		if(!$result = mysqli_query($conn, $sql)) {
			die("Error: $sql");
		}
		mysqli_close($conn);
		return $result;
	}

	function deleteLoan($book){
		$conn = connectDB();
		
		$split = explode("\n", $book);
		$book_id = $split[0];
		$user_id = $split[1];

		$sql = "DELETE FROM loan WHERE book_id = $book_id AND user_id = $user_id";
		
		if($result = mysqli_query($conn, $sql)) {
			echo "New record created successfully <br/>";
			header("Location: admin.php");
			} else {
			die("Error: $sql");
		}
		mysqli_close($conn);
	}

	function returnBook($book, $quantity){
		$conn = connectDB();

		$split = explode("\n", $book);
		$book_id = $split[0];
		$user_id = $split[1];

		$sql = "UPDATE book SET quantity=$quantity WHERE book_id=$book_id";
		
		if($result = mysqli_query($conn, $sql)) {
			echo "New record created successfully <br/>";
			header("Location: user.php");
			} else {
			die("Error: $sql");
		}
		mysqli_close($conn);
	}
	
	function detailBook($book){
		$conn = connectDB();
		$book_id = $book;
		
		header("Location: detail.php");
		
		mysqli_close($conn);
	}
	
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		if($_POST['command'] === 'insert') {
			insertBook();
		} else if($_POST['command'] === 'delete') {
			deleteBook($_POST['book_id']);
		} else if($_POST['command'] === 'update') {
			rentBook($_POST['book_id'],$_POST['quantity']-1);
			updateMyBook($_POST['book_id']);
		} else if($_POST['command'] === 'return') {
			deleteLoan($_POST['book_id']);
			returnBook($_POST['book_id'],$_POST['quantity']+1);
		} else if($_POST['command'] === 'detail') {
			session_start();
			$_SESSION['book_id'] = $_POST['book_id'];
			detailBook($_POST['book_id']);
		}
		
	}
	
	
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bookoo</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="src/css/style.css">
	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
		  	<div class="container-fluid">
		  		<div class="navbar-header">
		      		<a class="navbar-brand" href="#">Bookoo</a>
		    	</div>
		    	<ul class="nav navbar-nav navbar-right">
		    		<li>
		    			<a href="#">
						Welcome,
						<?php
							session_start();
							if (!isset($_SESSION["username"])){
								header("Location: index.php");
							}
							echo $_SESSION["username"];
						?>
						!
						</a>
					</li>
					<li>
						<a href="mybook.php">My Books</a>
					</li>
		      		<li>
		      			<a href="logout.php">
		      				<span class="glyphicon glyphicon-log-out"></span> Log Out
		      			</a>
		      		</li>
		    	</ul>
		  	</div>
		</nav>
		<div class="container" id="div2">
			<h2 class="text-center">Bookoo</h2>
			<h4>Book List</h4>
			<div class="table-responsive">
				<table class='table table-hover '>
					<thead > <tr> <th>ID</th> <th>Image</th> <th>Title</th> <th>Publisher</th> <th>Author</th> <th>Description</th> <th>Quantity</th> <th>Review</th> </tr> </thead>
					<tbody>
						<?php
							$books = selectAllFromTable("book");
							while ($row = mysqli_fetch_row($books)) {
								echo "<tr>";
								echo
								'<td class="table-bordered"><p>'.$row[0].'</p></td>
								<td class="table-bordered">
									<form action="user.php" method="post">
										<input type="hidden" id="detail-command" name="command" value="detail">
										<input type="hidden" id="detail-command" name="book_id" value="'.$row[0].'">
										<button type="submit">
										<img src="'.$row[1].'" alt="Image not available" style="width:100px">
										</button>
									</form>
								</td>
								<td class="table-bordered"><p>'.$row[2].'</p></td>
								<td class="table-bordered"><p>'.$row[3].'</p></td>
								<td class="table-bordered"><p>'.$row[4].'</p></td>
								<td class="table-bordered" style="text-align: justify;"><p>'.$row[5].'</p></td>
								<td class="table-bordered"><p>'.$row[6].'</p></td>';


								
								echo '<td><a href="#" data-toggle="modal" data-target="#reviewModal'.$row[0].'">See Review</a>';
								echo '
								<div id="reviewModal'.$row[0].'" class="modal fade" role="dialog">
								  <div class="modal-dialog modal-lg">

								    <div class="modal-content">
								      <div class="modal-header">
								        <button type="button" class="close" data-dismiss="modal">&times;</button>
								        <h4 class="modal-title">Modal Header</h4>
								      </div>
								      <div class="modal-body>
								      <ul>
								';
								$reviews = selectReview($row[0]);
								while ($reviewRow = mysqli_fetch_row($reviews)){
									echo '<li><p>'.$reviewRow[1].' on '.$reviewRow[2].'</p>
											<p>'.$reviewRow[3].'</p></li>';
								}
								echo '
								</ul>
								</div>
								      <div class="modal-footer">
								        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								      </div>
								    </div>

								  </div>
								</div>
								';
								echo 
								'</td>';


								if(checkBook($row[0])){
									echo 
									'<td>
										<form action="user.php" method="post">
											<input type="hidden" id="return-quantity" name="quantity" value="'.$row[6].'">
											<input type="hidden" id="return-bookid" name="book_id" value="'.$row[0]."\n".$_SESSION['user_id'].'">
											<input type="hidden" id="return-command" name="command" value="return">
											<button type="submit" class="btn btn-info"><span class="glyphicons glyphicons-circle-arrow-left"></span> Return this</button>
										</form>
									</td>';
									echo "</tr>";
								} else if ($row[6]>0){
									echo 
									'<td>
										<form action="user.php" method="post">
											<input type="hidden" id="update-quantity" name="quantity" value="'.$row[6].'">
											<input type="hidden" id="update-bookid" name="book_id" value="'.$row[0]."\n".$_SESSION['user_id'].'">
											<input type="hidden" id="update-command" name="command" value="update">
											<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"></span> Get this</button>
										</form>
									</td>';
									echo "</tr>";
								} else {
									echo "</tr>";
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>							
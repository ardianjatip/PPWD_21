<?php
	session_start();
	function connectDB() {
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "tugas_akhir";
		
		// Create connection
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		
		// Check connection
		if (!$conn) {
			die("Connection failed: " + mysqli_connect_error());
		}
		return $conn;
	}

	function selectAllFromTable($table) {
		$conn = connectDB();
		
		$sql = "SELECT book_id, img_path, title, publisher, author, description, quantity FROM $table";
		
		if(!$result = mysqli_query($conn, $sql)) {
			die("Error: $sql");
		}
		mysqli_close($conn);
		return $result;
	}

	function selectAllLoan(){
		$conn = connectDB();

		$userid = $_SESSION["user_id"];

		$sql = "SELECT book.book_id, book.img_path, book.title, book.publisher, book.author, book.description, book.quantity FROM `loan` INNER JOIN `book` on loan.book_id=book.book_id WHERE user_id = $userid";
		
		if(!$result = mysqli_query($conn, $sql)) {
			die("Error: $sql");
		}
		mysqli_close($conn);
		return $result;
	}

	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		if($_POST['command'] === 'return') {
			deleteLoan($_POST['book_id']);
			returnBook($_POST['book_id'],$_POST['quantity']+1);
		}
	}
			
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bookoo</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="src/css/style.css">
	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
		  	<div class="container-fluid">
		  		<div class="navbar-header">
		      		<a class="navbar-brand" href="#">Bookoo</a>
		    	</div>
		    	<ul class="nav navbar-nav navbar-right">
		    		<li>
		    			<a href="#">
						Welcome,
						<?php
							if (!isset($_SESSION["username"])){
								header("Location: index.php");
							}
							echo $_SESSION["username"];
						?>
						!
						</a>
					</li>
					<li>
						<a href="user.php">Home</a>
					</li>		
					<li>
		      			<a href="logout.php">
		      				<span class="glyphicon glyphicon-log-out"></span> Log Out
		      			</a>
		      		</li>
		    	</ul>
		  	</div>
		</nav>
		<div class="container" id="div2">
			<h2 class="text-center">Bookoo</h2>
			
			<h4>My Books</h4>
			
			<div class="table-responsive">
				<table class='table table-hover '>
					<thead > <tr> <th>ID</th> <th>Image</th> <th>Title</th> <th>Publisher</th> <th>Author</th> <th>Description</th> </tr> </thead>
					<tbody>
						<?php
							$books = selectAllLoan();
							while ($row = mysqli_fetch_row($books)) {
								echo "<tr>";
								echo
								'<td class="table-bordered"><p>'.$row[0].'</p></td>
								<td class="table-bordered"><img src="'.$row[1].'" alt="Image not available" style="width:100px"></td>
								<td class="table-bordered"><p>'.$row[2].'</p></td>
								<td class="table-bordered"><p>'.$row[3].'</p></td>
								<td class="table-bordered"><p>'.$row[4].'</p></td>
								<td class="table-bordered" style="text-align: justify;"><p>'.$row[5].'</p></td>';

								echo 
									'<td>
										<form action="user.php" method="post">
											<input type="hidden" id="return-quantity" name="quantity" value="'.$row[6].'">
											<input type="hidden" id="return-bookid" name="book_id" value="'.$row[0]."\n".$_SESSION['user_id'].'">
											<input type="hidden" id="return-command" name="command" value="return">
											<button type="submit" class="btn btn-info"><span class="glyphicons glyphicons-circle-arrow-left"></span> Return this</button>
										</form>
									</td>';
								echo "</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>							
<?php
	session_start();
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "tugas_akhir";
	$isAdmin = false;
	$isUser = false;

	// Create connection
	$conn = mysqli_connect($servername, $username, $password, $dbname);

	// Check connection
	if (!$conn) {
		die("Connection failed: " + mysqli_connect_error());
	}

	if($_SERVER["REQUEST_METHOD"] === "POST") {
		$username_input = $_POST["username"];
		$password_input = $_POST["password"];

		$sql_login = "SELECT * FROM user";
		$result = mysqli_query($conn, $sql_login);

		if(mysqli_num_rows($result) > 0) {
			while ($row = mysqli_fetch_assoc($result)) {
				if($username_input === $row["username"] && $password_input === $row["password"]) {
					if($row["role"] === "admin") {
						$_SESSION["username"] = $row["username"];
						$_SESSION["user_id"] = $row["user_id"];
						$isAdmin = true;
						break;
					}
					else if($row["role"] === "user") {
						$_SESSION["username"] = $row["username"];
						$_SESSION["user_id"] = $row["user_id"];
						$isUser = true;
						break;	
					}
				}
			}

			if($isAdmin) {
				header("Location: admin.php");
			} else if($isUser) {
				header("Location: user.php");
			} else {
				echo  "<script type='text/javascript'>alert('Login Gagal');</script>";
			}
		}	
	}
	
			
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bookoo</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="src/css/style.css">
	</head>
	<body>
		<nav class="navbar navbar-default">
		  	<div class="container-fluid">
		  		<div class="navbar-header">
		      		<a class="navbar-brand" href="#">Bookoo</a>
		    	</div>
		    	<ul class="nav navbar-nav navbar-right">
		    		<li><a href="guest.php">Our collections</a></li>
		      		<li><button id="login-btn" type="button" class="btn" data-toggle="modal" data-target="#loginModal">Login</button></li>
		    	</ul>
		  	</div>
		</nav>
		<div class="modal fade" id="loginModal" tabindex="0" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="insertModalLabel">Welcome! Please Login</h4>
					</div>
					<div class="modal-body">
						<form action="index.php" method="post">
							<div class="form-group">
								<label for="username">Username</label>
								<input type="text" class="form-control" id="insert-username" name="username" placeholder="Username">
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" class="form-control" id="insert-password" name="password" placeholder="Password">
							</div>
							<input type="hidden" id="insert-command" name="command" value="insert">
							<button type="submit" class="btn btn-info">Login</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div id="div1" class="text-center">
				<h1>Welcome to Bookoo!</h1>
				<p>There are hundreds of books ready to be booked by you!</p>
			</div>
		</div>

		<div class="container">
  		<br>
	  	<div id="myCarousel" class="carousel slide" data-ride="carousel">
	    <!-- Indicators -->
	    <ol class="carousel-indicators">
	    	<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	      	<li data-target="#myCarousel" data-slide-to="1"></li>
	      	<li data-target="#myCarousel" data-slide-to="2"></li>
	      	<li data-target="#myCarousel" data-slide-to="3"></li>
	    </ol>

	    <!-- Wrapper for slides -->
	    <div class="carousel-inner" role="listbox">

	    	<div class="item active">
	        	<img src="src/images/library.jpg" alt="Chania">
	        	<div class="carousel-caption">
	          		<h3>Tons of Books!</h3>
	          		<p>Find your books in our collections!</p>
	        	</div>
	      	</div>

		    <div class="item">
		    	<img src="src/images/happy.jpg" alt="Chania">
		        <div class="carousel-caption">
		          <h3>It's Free!</h3>
		          <p>Enjoy our books collection for free!</p>
		        </div>
		    </div>
		    
		    <div class="item">
		    	<img src="src/images/fine.jpg" alt="Flower">
		        <div class="carousel-caption">
		        	<h3>Fine Books</h3>
		          	<p>All of our books collection are in high quality.</p>
		        </div>
		    </div>

		    <div class="item">
		    	<img src="src/images/read.jpg" alt="Flower">
		        <div class="carousel-caption">
		          	<h3>Read Everywhere</h3>
		         	<p>You can enjoy our books in any places you like!</p>
		        </div>
		    </div>
	  
		</div>

	    <!-- Left and right controls -->
	    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
	      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	      <span class="sr-only">Previous</span>
	    </a>
	    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
	      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	      <span class="sr-only">Next</span>
	    </a>
	  </div>
	</div>

		<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>